import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Previewer } from 'pagedjs';
import { loremIpsum } from 'lorem-ipsum';

interface Section {
  title: string;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  title = 'ng-print-demo';
  sections: Section[] = [];
  print: boolean = false;

  constructor() {
    for (let i = 0; i < 10; i++) {
      this.sections.push({
        title: loremIpsum({ count: 1 }),
        text: loremIpsum({ count: 30 }),
      });
    }
  }

  ngAfterViewInit(): void {}

  onPrint(): void {
    const paged = new Previewer();
    const input = document.getElementById('contentInput');
    const output = document.getElementById('contentOutput');
    this.print = true;

    paged.preview(input, ['/assets/print.css'], output).then((flow: any) => {
      window.print();
      console.log({ flow });
      console.log('Rendered', flow.total, 'pages.');
    });
  }
}
